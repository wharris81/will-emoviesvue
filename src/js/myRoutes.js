import Index from '../components/Index.vue';
import Confirmation from '../components/Confirmation.vue';
import LoginPage from '../components/LoginPage.vue'

export default [
  // Redirects to /route-one as the default route.
  {
    path: '/',
    component: Index,
  },
  {
    path: '/Login',
    component: LoginPage
  },
  {
    path: '/Confirmation',
    component: Confirmation
  }
];