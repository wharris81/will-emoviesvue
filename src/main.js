import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import myRoutes from './js/myRoutes'

Vue.use(VueRouter);

const router = new VueRouter ({
  routes: myRoutes
});


Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
