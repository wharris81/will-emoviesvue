const path = require("path")
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
    entry: "./src/main.js",
    output: {
        path: path.resolve(__dirname, "/dist"),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {   test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {   test: /\.vue$/, use: 'vue-loader' },
            {   test: /\.js$/, use: 'babel-loader', exclude: /node_modules/}
        ]
    },
    plugins: [
        new VueLoaderPlugin()
        ]
};